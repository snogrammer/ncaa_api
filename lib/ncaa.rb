# frozen_string_literal: true

require 'active_support'
require 'active_support/inflector'
require 'active_support/core_ext/object'

require 'ncaa/version'
require 'ncaa/client/base'

require 'ncaa/base'
require 'ncaa/basketball'
require 'ncaa/football'

module Ncaa
  class Error < StandardError; end

  module_function

  # @param args [Hash]
  # @param args [Symbol] :type Sport type
  # @param args [Logger] :logger Defaults to Logger.new(STDOUT)
  # @param args [Integer] :timeout Defaults to 10
  # @return [Client::Base]
  def new(**args)
    client = Client::Base.new(**args)
    klass(**args).new(client: client, **args)
  end

  # @param args [Hash]
  # @param args [Symbol] :type Sport type
  # @return [Ncaa::Base]
  # @raise [ArgumentError] When sport is invalid
  def klass(**args)
    raise ArgumentError.new('type is required') if args[:type].blank?

    sport = args[:type].to_s.camelize
    type = "Ncaa::#{sport}".safe_constantize
    raise ArgumentError.new("#{type} is not a valid type") if type.nil?

    type
  end
end
