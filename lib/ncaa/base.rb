# frozen_string_literal: true

require 'ncaa/parser/html/base'

module Ncaa
  class Base
    SERVER = 'casablanca'
    CLIENT_URL = 'https://www.ncaa.com'

    attr_reader :client, :logger, :division, :sport

    # @param client [Ncaa::Client::Base]
    # @param division [String]
    #  basketball-men: d1, d2, d3
    #  football: fbs, fcs, d2, d3
    def initialize(client:, division: 'd1', **args)
      @client = client
      @logger = client.logger
      @division = division.downcase
      @sport = args[:type]
    end

    # @param date [Time] Must respond to DateTime methods: #year, #month, #day
    # @param options [Hash]
    # @param headers [Hash]
    # @return [Hash]
    # @see https://data.ncaa.com/casablanca/scoreboard/basketball-men/d3/2019/01/17/scoreboard.json
    def scoreboard(date = Time.now.utc, options: {}, headers: {})
      endpoint = "/#{SERVER}/scoreboard/#{sport_uri}/#{division}" \
                 "/#{date.year}/#{format_date(date.month)}/#{format_date(date.day)}/scoreboard.json"

      resp = client.get(endpoint, options: options, headers: headers)
      parse_json(resp.body)
    end

    alias games scoreboard
    alias schedule scoreboard

    # @param game_uri [String] Game uri
    # @param options [Hash]
    # @param headers [Hash]
    # @return [Hash]
    # @see https://data.ncaa.com/casablanca/game/3909829/boxscore.json
    def boxscore(game_uri, options: {}, headers: {})
      endpoint = "/#{SERVER}/#{game_uri}/boxscore.json"
      resp = client.get(endpoint, options: options, headers: headers)
      parse_json(resp.body)
    end

    alias game boxscore

    # @param game_uri [String] Game uri
    # @param options [Hash]
    # @param headers [Hash]
    # @return [Hash]
    # @see https://data.ncaa.com/casablanca/game/3909829/gameInfo.json
    def game_info(game_uri, options: {}, headers: {})
      endpoint = "/#{SERVER}/#{game_uri}/gameInfo.json"
      resp = client.get(endpoint, options: options, headers: headers)
      parse_json(resp.body)
    end

    alias info game_info

    # @param game_uri [String] Game uri
    # @param options [Hash]
    # @param headers [Hash]
    # @return [Hash]
    # @see https://data.ncaa.com/casablanca/game/3909829/pbp.json
    def play_by_play(game_uri, options: {}, headers: {})
      endpoint = "/#{SERVER}/#{game_uri}/pbp.json"
      resp = client.get(endpoint, options: options, headers: headers)
      parse_json(resp.body)
    end

    alias live play_by_play

    # @param type [String] Valid: associated-press, usa-today-coaches
    # @param options [Hash]
    # @param headers [Hash]
    # @see https://www.ncaa.com/rankings/basketball-men/d1/ncaa-mens-basketball-net-rankings
    def rankings(type = 'ncaa-mens-basketball-net-rankings', options: {}, headers: {})
      endpoint = "/rankings/#{sport_uri}/#{division}/#{type}"
      resp = client_request do
        client.get(endpoint, options: options, headers: headers)
      end
      Ncaa::Parser::Html::Rankings.parse(resp)
    end

    # @param options [Hash]
    # @param headers [Hash]
    # @see https://www.ncaa.com/standings/basketball-men/d1/all-conferences
    def standings(options: {}, headers: {})
      endpoint = "/standings/#{sport_uri}/#{division}/all-conferences"
      resp = client_request do
        client.get(endpoint, options: options, headers: headers)
      end
      Ncaa::Parser::Html::Standings.parse(resp)
    end

    # @param options [Hash]
    # @param headers [Hash]
    # @return [Hash]
    # @see # https://www.ncaa.com/json/schools
    def schools(options: {}, headers: {})
      endpoint = '/json/schools'
      resp = client_request do
        client.get(endpoint, options: options, headers: headers)
      end
      parse_json(resp.body)
    end

    alias teams schools

    protected

    def sport_uri
      raise NotImplementedError.new('sport_uri needs to be implemented')
    end

    private

    # @param response [String]
    # @return [Hash]
    def parse_json(response, symbolize: true)
      JSON.parse(response, symbolize_names: symbolize)
    rescue StandardError => e
      raise InvalidJsonResponseError.new(error: 'Invalid Json', response: response, message: e.message)
    end

    def format_date(number, formatter: '%02d')
      format(formatter, number)
    end

    # Request html url for parsing non-Json endpoints for data
    # Resets client to data Json url after block is completed
    def client_request
      data_url = client.host
      client.host = CLIENT_URL
      results = yield
      client.host = data_url
      results
    end

    class InvalidJsonResponseError < StandardError; end
  end
end
