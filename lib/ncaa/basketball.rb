# frozen_string_literal: true

module Ncaa
  class Basketball < Base
    DIVISIONS = %w[d1 d2 d3].freeze
    RANKINGS = [
      D1 = %w[associated-press ncaa-mens-basketball-net-rankings usa-today-coaches].freeze,
      D2 = %w[nabc-coaches regional-ranking d2sida].freeze,
      D3 = %w[d3hoopscom regional-rankings-0].freeze
    ].flatten.freeze

    def metadata
      {
        divisions: DIVISIONS,
        rankings: {
          d1: D1,
          d2: D2,
          d3: D3
        }
      }
    end

    protected

    def sport_uri
      'basketball-men'
    end
  end
end
