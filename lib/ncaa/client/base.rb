# frozen_string_literal: true

require 'ncaa/client/result'
require 'rest_client'

module Ncaa
  module Client
    class Error < RuntimeError
      def initialize(message)
        super(message)
      end
    end

    class Base
      BASE_URL = 'https://data.ncaa.com' # JSON API; Non-JSON base is https://www.ncaa.com
      DEFAULT_TIMEOUT = 10

      attr_accessor :host # able to update to www endpoint for scraping
      attr_reader :logger

      def initialize(logger: Logger.new(STDOUT), timeout: DEFAULT_TIMEOUT, **_args)
        @host = BASE_URL
        @logger = logger
        @timeout = timeout
      end

      # GET request
      #
      # @param uri [String]
      # @param options [Hash]
      # @param headers [Hash]
      # @return [Result]
      def get(uri, options: {}, headers: {})
        request do
          url = host + normalize_uri(uri)
          options ||= {}
          headers ||= {}

          query_params = CGI.unescape(options.to_query)
          url += ('?' + query_params) unless query_params.blank?

          response = RestClient::Request.execute(method: :get,
                                                 url: url,
                                                 headers: default_headers.merge(headers),
                                                 open_timeout: open_timeout,
                                                 timeout: timeout)

          logger.debug(message: 'GET Request', url: url, options: options, status: response.code)
          success?(response.code) ? Result.success(response) : Result.failed(response)
        end
      end

      # @yield [Result]
      def request
        result = yield

        if result.success?
          logger.debug(status: result.code, body: result.body)
        else
          logger.warn(error: 'Request Error', status: result.code, body: result.body)
        end

        result
      rescue ::RestClient::GatewayTimeout
        raise Error.new('Gateway timeout')
      rescue ::RestClient::RequestTimeout
        raise Error.new('Request timeout')
      rescue ::RestClient::Exception => e
        handle_error(e)
      end

      # Handle errors
      # @param error [Error]
      # @return [Result]
      def handle_error(error)
        response = error.response
        message = error.message
        logger.error(error: 'Request Error', code: response.code, message: message)
        Result.failed(response)
      end

      # @return [Hash]
      def default_headers
        agent = "ncaa/#{Ncaa::VERSION} "\
                "(#{RestClient::Platform.architecture}) #{RestClient::Platform.ruby_agent_version}"

        {
          'User-Agent' => agent
        }
      end

      # @return [Integer]
      def timeout
        @timeout || DEFAULT_TIMEOUT
      end

      # @return [Integer]
      def open_timeout
        @open_timeout || DEFAULT_TIMEOUT
      end

      private

      # @return [Boolean]
      def success?(status_code = 0)
        return true if status_code.in?(200..299)

        false
      end

      # @param uri [String]
      # @return [String]
      def normalize_uri(uri)
        uris = uri.split('/').delete_if(&:blank?)
        '/' + uris.join('/')
      end
    end
  end
end
