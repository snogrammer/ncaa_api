# frozen_string_literal: true

module Ncaa
  class Football < Base
    DIVISIONS = %w[fbs fcs d2 d3].freeze
    RANKINGS = [
      FBS = %w[associated-press college-football-playoff usa-today-coaches-poll].freeze,
      FCS = %w[fcs-coaches-poll stats-fcs-top-25 simple-ratings-system].freeze,
      D2 = %w[regional-rankings afca-coaches d2footballcom].freeze,
      D3 = %w[regional-rankings afca-coaches d3footballcom].freeze
    ].flatten.freeze

    # @param year_week [String] Example: 2019/15
    # @see https://data.ncaa.com/casablanca/scoreboard/football/fbs/2019/15/scoreboard.json
    def scoreboard(year_week = handle_current_week, options: {}, headers: {})
      endpoint = "/#{SERVER}/scoreboard/#{sport_uri}/#{division}/#{year_week}/scoreboard.json"
      resp = client.get(endpoint, options: options, headers: headers)
      parse_json(resp.body)
    end

    def current_week(options: {}, headers: {})
      endpoint = "/#{SERVER}/schedule/#{sport_uri}/#{division}/today.json"
      resp = client.get(endpoint, options: options, headers: headers)
      parse_json(resp.body)
    end

    def metadata
      {
        divisions: DIVISIONS,
        rankings: {
          fbs: FBS,
          fcs: FCS,
          d2: D2,
          d3: D3
        }
      }
    end

    protected

    def sport_uri
      'football'
    end

    private

    def handle_current_week
      current_week.dig(:today).strip
    end
  end
end
