# frozen_string_literal: true

require 'nokogiri'

module Ncaa
  module Parser
    module Html
      class Base
        attr_reader :body, :code, :html, :response, :options

        def self.parse(resp, options: {})
          response = new(resp, options: options)
          response.build
        end

        def initialize(resp, options: {})
          @body = resp.body
          @code = resp.code
          @html = parse_html
          @response = resp
          @options = options
        end

        def build
          raise NotImplementedError.new
        end

        private

        # @param response [String]
        # @return [Nokogiri::HTML]
        def parse_html
          Nokogiri::HTML(body)
        rescue StandardError => e
          raise InvalidHtmlResponseError.new(error: 'Invalid Html', response: response, message: e.message)
        end

        class InvalidHtmlResponseError < StandardError; end
      end
    end
  end
end

# require after Base is defined
require 'ncaa/parser/html/rankings'
require 'ncaa/parser/html/standings'
