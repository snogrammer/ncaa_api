# frozen_string_literal: true

module Ncaa
  module Parser
    module Html
      class Rankings < Base
        def build
          {
            type: type.inner_html.strip,
            uri: type.values.first.strip,
            last_updated: last_updated,
            data: rankings
          }
        end

        private

        # @return [Array<Nokogiri::HTML>]
        def table
          @table ||= html.css('article.rankings-content table')
        end

        # @return [Array<Symbol>]
        def keys
          @keys ||= table.css('thead th').map { |x| x.text.remove(/\s/).underscore.to_sym }
        end

        # @return [Nokogiri::HTML]
        def type
          @type ||= html.css('form#rankings-menu-form select#edit-rankings-menu option[selected="selected"]').first
        end

        # @return [Time]
        def last_updated
          @last_updated ||=
            begin
              updated = html.css('figure.rankings-last-updated').text
              date = updated[/\w+{3,}\.? \d+{1,2}\,? \d+{2,4}/]
              Time.parse(date)
            rescue TypeError
              nil
            end
        end

        # @return [Array<Hash>]
        def rankings
          @rankings ||= begin
            table.css('tbody tr').each.with_object([]) do |row, arr|
              team = {}
              columns = row.css('td')

              keys.each.with_index { |key, i| team[key] = columns[i].text }

              arr << team
            end
          end
        end
      end
    end
  end
end
