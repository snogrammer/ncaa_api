# frozen_string_literal: true

module Ncaa
  module Parser
    module Html
      class Standings < Base
        def build
          {
            type: 'standings',
            sort: 'by_conference',
            last_updated: last_updated,
            data: standings
          }
        end

        private

        def conferences
          @conferences ||= html.css('figure.standings-conference')
        end

        def table(conference)
          conference.next_element.css('table')
        end

        def team_rows(table)
          table.css('tbody tr')
        end

        # @return [Array<Symbol>]
        def keys(table)
          @keys ||= table.css('thead th').map { |x| x.text.remove(/\s/).underscore.to_sym }
        end

        # @return [Time]
        def last_updated
          @last_updated ||= begin
            updated = html.css('figure.standings-last-updated').text
            date = updated[/\w+{3,}\.? \d+{1,2}, \d+{2,4} .+/]
            Time.parse(date)
          end
        end

        def team_standing(row)
          standings = row.css('td')

          {
            school: standings[0].text.strip,
            school_image_url: standings[0].css('img').attr('src').value,
            conference: {
              wins: standings[1].text.to_i,
              losses: standings[2].text.to_i,
              percentage: standings[3].text.to_f
            },
            overall: {
              wins: standings[4].text.to_i,
              losses: standings[5].text.to_i,
              percentage: standings[6].text.to_f
            },
            streak: standings[7].text
          }
        end

        # @return [Array<Hash>]
        def standings
          @standings ||= begin
            conferences.each.with_object([]) do |conference, arr|
              standing = {
                conference: conference.text.strip,
                conference_image_url: conference.css('img').attr('src').value,
                standings: []
              }

              conference_table = table(conference)
              teams = team_rows(conference_table)

              teams.each { |team| standing[:standings] << team_standing(team) }

              arr << standing
            end
          end
        end
      end
    end
  end
end
