# frozen_string_literal: true

RSpec.describe Ncaa do
  let(:type) { :basketball }

  it 'has a version number' do
    expect(Ncaa::VERSION).not_to be nil
  end

  describe '.new' do
    it 'returns sport client' do
      sub = described_class.new(type: type)
      expect(sub).to be_a(Ncaa::Basketball)
      expect(sub.client).to be_a(Ncaa::Client::Base)
    end
  end

  describe '.klass' do
    it 'returns valid class' do
      sub = described_class.klass(type: type)
      expect(sub).to eq(Ncaa::Basketball)
    end

    it 'raises error when type is missing' do
      expect { described_class.klass }.to raise_error(ArgumentError)
    end

    it 'raises error when type is invalid' do
      expect { described_class.klass(type: 'foobar') }.to raise_error(ArgumentError)
    end
  end
end
